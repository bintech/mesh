CXXFLAGS =	-O2 -g -Wall -fmessage-length=0

OBJS =		game_mesh.o	\
			src/data/map.o	\
			src/mesh/Mesh.o	\
			src/mesh/ObjectType.o	\
			src/mesh/Vector.o	\
			src/mesh/MeshMain.o	\
			src/mesh/stringtools.o	\
			src/mesh/WaveFrontLoader.o	

LIBS =

TARGET =	game_mesh.exe

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
