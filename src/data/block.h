/*
 * block.h
 *
 *  Created on: 25.03.2015
 *      Author: neal
 *      Edited by binchi
 */


#ifndef BLOCK_HPP_
#define BLOCK_HPP_


enum block {
  AIR   = 0,
  DIRT  = 1,
  ROCK  = 2,
  GREEN = 3,
  PATH  = 4,


  // static objects
  GRASS = 64,
  TREE  = 65,

  // dynamic objects
  PLAYER = 128,
  ENEMY  = 129,

};

#endif
