/*
 * map.h
 *
 *  Created on: 25.03.2015
 *      Author: neal
 *      Edited by binchi
 */


#include <iostream>

#include "map.h"

Map::Map()
{
  data = (enum block*) calloc(sizeof(*data), BLOCKS_X * BLOCKS_Y * BLOCKS_Z);

  //init testdata
  for (int x = 0; x < WORLDSIZE; x++)
    {
      for (int y = 0; y < WORLDSIZE; y++)
        {
          for (int z = 0; z < WORLDSIZE; z++)
            {
              world[x][y][z] = AIR;
            }
        }
    }

  for (int x = 0; x < WORLDSIZE; x++)
    {
      for (int z = 0; z < WORLDSIZE; z++)
        {
          world[x][0][z] = DIRT;
        }
    }


  //test optimizing
  {
	  int n=4;
	  int nx1=1;
	  int nx2=nx1+n;
	  int ny1=4;
	  int ny2=ny1+n;
	  int nz1=2;
	  int nz2=nz1+n;
	  for (int x = nx1; x < nx2; x++){
		  for (int y = ny1; y < ny2; y++){
			for (int z = nz1; z < nz2; z++){
				world[x][y][z] = GREEN;
			  }
		  }
	  }
  }
  {
	  int n=2;
	    int nx1=4;
	    int nx2=nx1+n;
	    int ny1=2;
	    int ny2=ny1+n;
	    int nz1=1;
	    int nz2=nz1+n;
	    for (int x = nx1; x < nx2; x++){
	  	  for (int y = ny1; y < ny2; y++){
	          for (int z = nz1; z < nz2; z++){
	              world[x][y][z] = ROCK;
	            }
	        }
	    }
  }

  //world[3][1][2] = TREE;
}

enum block
Map::getBlock(int x, int y, int z)
{
  return world[x][y][z];
}
void
Map::setBlock(int x, int y, int z,enum block b)
{
  world[x][y][z]=b;
}

