/*
 * map.h
 *
 *  Created on: 25.03.2015
 *      Author: neal
 *      Edited by binchi
 */


#ifndef MAP_HPP_
#define MAP_HPP_

#include <stdlib.h>
#include "block.h"
#define BLOCKS_X 32
#define BLOCKS_Y 16
#define BLOCKS_Z 32

//#include "map.cpp"

#define WORLDSIZE 8 //number of blocks the world is height/width/deep


class Map {
  private:
    enum block* data;
//    enemy*  enemy;
//    player* player;
    enum block world[WORLDSIZE][WORLDSIZE][WORLDSIZE];

  public:
    Map();
    enum block getBlock(int x, int y, int z);
    void setBlock(int x, int y, int z,enum block b);

//    payer getPlayer(int nr);
//    enemy getEnemy(int nr);

};

#endif
