/*
 * mesh.hpp
 *
 *  Created on: 25.03.2015
 *      Author: binchi
 */

#ifndef MESHMAIN_HPP_
#define MESHMAIN_HPP_

#include <vector>

#include "../data/map.h"
#include "ObjectType.hpp"
#include "Mesh.hpp"

#define BOXSIZE 2 //lenght/width/deepth of each single block in the coord.system

class MeshMain {
  private:
    Map* map;
    std::vector<ObjectType*> obj_types;

    void generateWorld();
    Mesh* paintMesh(int x,int y,int z,char* mesh);
    Mesh* paintBox(int x,int y,int z,int blocksize);
    int getOptimizedBoxSize(int x,int y,int z);
    ObjectType* getObjectType(block type);
    void initTypes();
    void printoutWorld();


  public:
    MeshMain(Map* mapNew);//interface for data (from Neal)


};

#endif /* MESHMAIN_HPP_ */
