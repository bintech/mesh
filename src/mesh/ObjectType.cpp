/*
 * ObjectType.hpp
 *
 *  Created on: 25.03.2015
 *      Author: binchi
 */
#include <string.h>
#include <stdlib.h>

#include "ObjectType.hpp"

ObjectType::ObjectType(enum block ntype,char* nmesh,char* ntexture){
	type=ntype;
	texture=ntexture;
	mesh=nmesh;
}
enum block ObjectType::getType(){
	return type;
}
char* ObjectType::getMesh(){
	return mesh;
}
char* ObjectType::getTexture(){
	return texture;
}
