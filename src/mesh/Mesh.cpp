/*
 * Mesh.cpp
 *
 *  Created on: 26.03.2015
 *      Author: binchi
 */

/** (kleine Hilfe) Zeiger:
 * int *a;
 * int b=5;
 *
 * a=&b;
 * println(a); // xxx (address of b)
 * println(*a); //5
 *
 * b=8;
 * println(*a); //8
 *
 * int c=6;
 * a=&c;
 * println(*a); //6
 *
 * b=*a; //b bekommt wert von a
 * println(b); //6
 * b=9;
 * println(*a); //6
 *
 */

#include <iostream>

#include "Mesh.hpp"

Mesh::Mesh(std::vector<Vector3f*>* nverts, std::vector<int>* nindex,
    std::vector<Vector2f*>* ntex)
{
  vertices = *nverts;
  indexes = *nindex;
  tex_coord = *ntex;

  //printout the vertices for testing
  /*for (int i = 0; i < vertices.size(); i++)
    {
      std::cout << "V: " << vertices[i]->x << ", " << vertices[i]->y << ", "
          << vertices[i]->z << std::endl;
    }*/

}
Mesh::Mesh(std::vector<Vector3f*>* nverts, std::vector<int>* nindex,
    std::vector<Vector2f*>* ntex,std::vector<Vector3f*>* nnorm)
{
  vertices = *nverts;
  indexes = *nindex;
  tex_coord = *ntex;
  normals = *nnorm;

  //printout the vertices for testing
  /*for (int i = 0; i < vertices.size(); i++)
    {
      std::cout << "V: " << vertices[i]->x << ", " << vertices[i]->y << ", "
          << vertices[i]->z << std::endl;
    }*/

}
void Mesh::setPosition(Vector3f* pos){
  position=pos;
}
