/*
 * Vector.cpp
 *
 *  Created on: 26.03.2015
 *      Author: binchi
 */

#include "Vector.hpp"

Vector3f::Vector3f()
{
  x = 0;
  y = 0;
  z = 0;
}
Vector3f::Vector3f(double nx, double ny, double nz)
{
  x = nx;
  y = ny;
  z = nz;
}
Vector2f::Vector2f()
{
  x = 0;
  y = 0;
}
Vector2f::Vector2f(double nx, double ny)
{
  x = nx;
  y = ny;
}
