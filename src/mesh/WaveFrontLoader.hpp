/*
 * WaveFrontLoader.hpp
 *
 *  Created on: 27.03.2015
 *      Author: binchi
 */

#ifndef WAVEFRONTLOADER_HPP_
#define WAVEFRONTLOADER_HPP_

#include <string>

#include "Mesh.hpp"

/**
 * Allows to load meshes from Wavefront (.obj) files.
 * Hint: Wavefront (.obj) is a fileformat for 3D-object meshes. It can be loaded in blender and has an "easy" to read textformat.
 * It mainly consists of 4 different line-types:
 * - Vertices: lines start with "v", e.g.: "v -0.300000 0.000000 -0.150000"
 * - Texture Coordinates: lines start with "vt", e.g.: "vt 0.054773 0.124317"
 * - Normal Vectors: lines start with "vn", e.g.: "vn -0.997900 0.065500 0.000000"
 * - Faces: lines start with "f", e.g.: "f 226/50/218 225/1/218 227/54/218"
 * Consider that the linenumbers of the different types must be suitable to the other types.
 *
 * A Wavefront file is often used in combination with an .mtl file which describes the material. This .mtl file is not always needed.
 */
class WaveFrontLoader
{
private:
  std::vector<int> indexes;
  std::vector<Vector3f*> faces;
  std::vector<Vector3f*> vertices;
  std::vector<Vector2f*> tex_coord;
  std::vector<Vector3f*> normals;

  Vector3f* getFace(int vertnr);
  void sortTexCoordAndNormals();
  void
  parseFaceLine(char* line);
  Vector2f*
  parseToVector2f(char* line);
  Vector3f*
  parseToVector3f(char* line);
  std::vector<std::string>
  splitBySpaces(char* line);

public:
  WaveFrontLoader();
  Mesh*
  loadMesh(char* path);

};

#endif
