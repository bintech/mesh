/*
 * mesh.cpp
 *
 *  Created on: 25.03.2015
 *      Author: binchi
 */

#include <iostream>
#include <stdio.h>

#include "MeshMain.hpp"
#include "WaveFrontLoader.hpp"

MeshMain::MeshMain(Map* mapNew)
{
  map = mapNew;
  initTypes();
  generateWorld();
}
/**
 * Inits the different blocktypes and sets their texture and mesh.
 */
void
MeshMain::initTypes()
{
  //doesn't need to init AIR, because it is never drawn (see Mesh.generateWorld()).
    {
      ObjectType* obj = new ObjectType(DIRT, NULL, "texture.png");
      obj_types.push_back(obj);
    }
    {
	  ObjectType* obj = new ObjectType(GREEN, NULL, "texture.png");
	  obj_types.push_back(obj);
	}
    {
	  ObjectType* obj = new ObjectType(PATH, NULL, "texture.png");
	  obj_types.push_back(obj);
	}
    {
	  ObjectType* obj = new ObjectType(ROCK, NULL, "texture.png");
	  obj_types.push_back(obj);
	}

    {
	  ObjectType* obj = new ObjectType(GRASS, "mesh.obj", "texture.png");
	  obj_types.push_back(obj);
	}
    {
      ObjectType* obj = new ObjectType(TREE, "src/baum13.obj", "texture.png");
      obj_types.push_back(obj);
    }

}
ObjectType*
MeshMain::getObjectType(enum block type)
{
  for (int i = 0; i < obj_types.size(); i++)
    {
      if (obj_types[i]->getType() == type)
        {
          return obj_types[i];
        }
    }
  return NULL;
}
void MeshMain::printoutWorld(){
	// paint z-schichten nebeneinander
	for (int y = WORLDSIZE-1; y >=0; y--)
		{
		for (int z = 0; z < WORLDSIZE; z++)
			{
			  for (int x = 0; x < WORLDSIZE; x++)
				{
					enum block type1 = map->getBlock(x, y, z);
					printf("%d",type1);
				}
			  printf("     ");
			}
			printf("\n");
		}
}
/**
 * Generates for each (x,y,z)-coordinate the block.
 */
void
MeshMain::generateWorld()
{
	printoutWorld();

  for (int x = 0; x < WORLDSIZE; x++)
    {
      for (int y = 0; y < WORLDSIZE; y++)
        {
          for (int z = 0; z < WORLDSIZE; z++)
            {
              enum block type = map->getBlock(x, y, z);
              if (type != AIR)
                {
                  if (getObjectType(type) == NULL)
                    {
                      return;
                    }
                  char* meshpath = getObjectType(type)->getMesh();
                  Mesh* mesh = NULL;
                  //meshpath==NULL -> ist box
                  if (meshpath == NULL)
                    {
                      mesh = paintBox(x, y, z, getOptimizedBoxSize(x, y, z));
                    }
                  else if (type == PLAYER)
                    {
                      //figures aus mehreren bloecken

                    }
                  else if (type == ENEMY)
                    {

                    }
                  else
                    {//other mesh -> load mesh
                      //mesh = paintMesh(x, y, z, meshpath);
                    }

                  if (mesh != NULL)
                    {
                      //each mesh is set to a position in the world
                      mesh->setPosition(new Vector3f(x,y,z));
                      //TODO:add mesh to world

                    }
                }
              else
                {
                  //AIR is not drawn
                  //std::cout<<"AIR"<<std::endl;
                }
            }
        }
    }
}
/**
 * Counts the blocks next to this one at the position (x,y,z) and scales it to the size of
 * n blocks (n -> return value). If -1 is returned, this block is already optimized in
 * another block and shouldn't be painted anymore. 1 means that the block can't be optimized.
 * x	x-position
 * y	y-position
 * z	z-position
 * return	The number of blocks which can be matched together to a block at this position.
 */
int
MeshMain::getOptimizedBoxSize(int x, int y, int z)
{
  if(x==(WORLDSIZE-1)||y==(WORLDSIZE-1)||z==(WORLDSIZE-1)){
	  //letzter block in reihe -> can't be optimized
	  return 1;
  }
  enum block type = map->getBlock(x, y, z);
  int n=1;
  while(true){
	  for(int nx=x;nx<x+n;nx++){
		  for(int ny=y;ny<y+n;ny++){
			  for(int nz=z;nz<z+n;nz++){
				  if(map->getBlock(nx, ny, nz)!=type
						  ||nx>=WORLDSIZE||ny>=WORLDSIZE||nz>=WORLDSIZE){
					//can't optimizing more
					n=n-1;
					if(n>1){
						for(int nnx=x;nnx<x+n;nnx++){
						  for(int nny=y;nny<y+n;nny++){
							  for(int nnz=z;nnz<z+n;nnz++){
								  if(nnx==x&&nny==y&&nnz==z){
									  //leaf first block
								  }
								  else{
									  //optimized blocks are set AIR, so they won't be created anymore
									  map->setBlock(nnx, nny, nnz,AIR);
								  }
							  }
						  }
					  }
					  std::cout<<"n: "<<x<<","<<y<<","<<z<<": "<<n<<std::endl;
					  printoutWorld();
					}
					return n;
				  }
			  }
		  }
	  }
	  n++;
  }
  //should never be reached
}
/**
 * Creates the mesh of one block at the given position.
 * x	x position in coord.system
 * y	y position in coord.system
 * z	z position in coord.system
 * mesh	Path to the meshfile.
 */
Mesh*
MeshMain::paintMesh(int x, int y, int z, char* path)
{
  //load mesh from file
  WaveFrontLoader* wl=new WaveFrontLoader();
  Mesh* mesh=wl->loadMesh(path);
  return mesh;
}

/**
 * Create a Boxmesh with texture coordinates (and normals). The midpoint of the box is
 * always the left edge down on the frontside. (easier to handle in the coord.system)
 * The box consists of triangles only.
 * x	Start x
 * y	Start y
 * z	Start z
 * blocksize	scale block
 */
Mesh*
MeshMain::paintBox(int x, int y, int z, int blocksize)
{
  if (blocksize != -1)
    {
      //create boxmesh: vertices, indexes,texture coord.,(normals)
      //TODO: optimize: only make seen sides
      /*bool sides[6];
       //0 unten
       //1 oben
       //2 links
       //3 rechts
       //4 vorne
       //5 hinten
       for(int i=0;i<6;i++){
       side[i]=true;
       }
       if(y==0){//boden -> lasse untere seite weg
       side[0]=false;
       }
       else if(y==WORLDSIZE-1){//oberste ebene -> lasse obere seite weg
       side[1]=false;
       }
       if(x==0){//linke seite weglassen
       side[2]=false;
       }
       else if(x==WORLDSIZE-1){//rechte seite weglassen
       side[3]=false;
       }
       if(z==0){//vordere seite weglassen
       side[4]=false;
       }
       else if(z==WORLDSIZE-1){//hintere seite weglassen
       side[5]=false;
       }*/

      /*coordinaten(front):
       0,3,0--3,3,0
       | \        |
       |   \      |
       |     \    |
       |       \  |
       |         \|
       0,0,0--3,0,0

       indexes:
       2\2--3
       | \  | Counter-clockwise
       |  \ |
       0--1\1
       */

      std::vector<Vector3f*> vertices;
      double bplus=(double)BOXSIZE/2 * blocksize;
      double bneg=-1*(double)BOXSIZE/2 * blocksize;
      vertices.push_back(new Vector3f(bneg, bneg, bneg));//0
      vertices.push_back(new Vector3f(bplus, bneg, bneg));//1
      vertices.push_back(new Vector3f(bneg, bplus, bneg));//2
      vertices.push_back(
          new Vector3f(bplus, bplus, bneg));//3

      vertices.push_back(new Vector3f(bneg, bneg, bplus));//4
      vertices.push_back(
          new Vector3f(bplus, bneg, bplus));//5
      vertices.push_back(
          new Vector3f(bneg, bplus, bplus));//6
      vertices.push_back(
          new Vector3f(bplus, bplus,bplus));//7


      std::vector<Vector2f*> texCoord;
      texCoord.push_back(new Vector2f(0, 0));
      texCoord.push_back(new Vector2f(blocksize, 0));
      texCoord.push_back(new Vector2f(0, blocksize));
      texCoord.push_back(new Vector2f(blocksize, blocksize));

      texCoord.push_back(new Vector2f(0, 0));
      texCoord.push_back(new Vector2f(blocksize, 0));
      texCoord.push_back(new Vector2f(0, blocksize));
      texCoord.push_back(new Vector2f(blocksize, blocksize));

      std::vector<int> indexes;
      indexes.push_back(1);
      indexes.push_back(0);
      indexes.push_back(2);//vorne    2,0,1, 1,3,2, auf anderer seite sichtbar
      indexes.push_back(2);
      indexes.push_back(3);
      indexes.push_back(1);

      indexes.push_back(6);
      indexes.push_back(4);
      indexes.push_back(5);//hinten
      indexes.push_back(5);
      indexes.push_back(7);
      indexes.push_back(6);

      indexes.push_back(2);
      indexes.push_back(0);
      indexes.push_back(6);//links
      indexes.push_back(6);
      indexes.push_back(0);
      indexes.push_back(4);

      indexes.push_back(3);
      indexes.push_back(7);
      indexes.push_back(1);//rechts
      indexes.push_back(1);
      indexes.push_back(7);
      indexes.push_back(5);

      indexes.push_back(6);
      indexes.push_back(7);
      indexes.push_back(2);//oben
      indexes.push_back(2);
      indexes.push_back(7);
      indexes.push_back(3);

      indexes.push_back(4);
      indexes.push_back(0);
      indexes.push_back(1);//unten
      indexes.push_back(4);
      indexes.push_back(1);
      indexes.push_back(5);

      /*int* indexes = { 1,0,2, 2,3,1, //vorne    2,0,1, 1,3,2, auf anderer seite sichtbar
       6,4,5, 5,7,6, //hinten
       2,0,6, 6,0,4, //links
       3,7,1, 1,7,5, //rechts
       6,7,2, 2,7,3, //oben
       4,0,1, 4,1,5};//unten
       */
      return new Mesh(&vertices, &indexes, &texCoord);
    }
  else
    {
      //optimized block next to it, don't draw this one
    }
}

