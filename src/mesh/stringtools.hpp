/*
 * stringtools.hpp
 *
 *  Created on: 28.03.2015
 *      Author: binchi
 */

#ifndef STRINGTOOLS_HPP_
#define STRINGTOOLS_HPP_

#include <vector>
#include <string>

/**
 * Some useful functions for strings and char*, so you don't have to search.
 * Hint:
 * - int atoi(char*): char* to int
 * - double atof(char*): char* to double
 */

bool
endsWith(const std::string &str, const std::string &suffix);
bool
startsWith(char* line, char* prefix);
bool
equals(char* a, char* b);
std::vector<char*>
splitByDelimiter(char* line, char* delimiter);
char*
toChar(std::string s);
std::string
toString(char* s);

#endif /* STRINGTOOLS_HPP_ */
