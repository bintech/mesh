/*
 * Mesh.hpp
 *
 *  Created on: 26.03.2015
 *      Author: binchi
 */

#ifndef MESH_HPP_
#define MESH_HPP_

#include <vector>

#include "Vector.hpp"

class Mesh
{
private:
  //the vertices
  std::vector<Vector3f*> vertices;
  /**the faces connections as triangles: contains only the nr. of the vertices, each 3 indexes are one triangle
   * z.B.: indexes[0],indexes[1],indexes[2] are one triangle
   * indexes[3],indexes[4],indexes[5] are another triangle
   */
  std::vector<int> indexes;
  //the texture coordinates. each vector describes one face. sorted by the indexes.
  std::vector<Vector2f*> tex_coord;
  //the normal vectors for shading
  std::vector<Vector3f*> normals;

  Vector3f* position;

public:
  Mesh(std::vector<Vector3f*>* nverts, std::vector<int>* nindex,
      std::vector<Vector2f*>* ntex);
  Mesh(std::vector<Vector3f*>* nverts, std::vector<int>* nindex,
      std::vector<Vector2f*>* ntex, std::vector<Vector3f*>* nnorm);
  void setPosition(Vector3f* pos);

};

#endif /* MESH_HPP_ */
