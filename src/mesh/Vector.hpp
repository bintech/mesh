/*
 * Vector.hpp
 *
 *  Created on: 26.03.2015
 *      Author: binchi
 */

#ifndef VECTOR_HPP_
#define VECTOR_HPP_

class Vector3f {
  private:

  public:
	double x;
	double y;
	double z;

    Vector3f();
    Vector3f(double nx,double ny,double nz);
};
class Vector2f {
  private:

  public:
	double x;
	double y;

    Vector2f();
    Vector2f(double nx,double ny);
};

#endif /* VECTOR_HPP_ */
