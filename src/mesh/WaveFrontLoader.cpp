/*
 * WaveFrontLoader.cpp
 *
 *  Created on: 27.03.2015
 *      Author: binchi
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "WaveFrontLoader.hpp"
#include "stringtools.hpp"

WaveFrontLoader::WaveFrontLoader()
{

}

Mesh*
WaveFrontLoader::loadMesh(char* path)
{
  std::cout << "load mesh: " << path << std::endl;
  if (!endsWith(path, ".obj"))
    {
      return NULL;
    }

  //read from file
  int size = 1024, pos;
  int c;
  char *buffer = (char *) malloc(size);

  FILE *f = fopen(path, "r");
  if (f)
    {
      do
        { // read all lines in file
          pos = 0;
          do
            { // read one line
              c = fgetc(f);
              if (c != EOF)
                buffer[pos++] = (char) c;
              if (pos >= size - 1)
                { // increase buffer length - leave room for 0
                  size *= 2;
                  buffer = (char*) realloc(buffer, size);
                }
            }
          while (c != EOF && c != '\n');
          buffer[pos] = 0;
          // line is now in buffer -> handle line
          //is vertice line?
          if (startsWith(buffer, "v "))
            {
              //parse into 3 doubles
              vertices.push_back(parseToVector3f(buffer));
            }
          //is normal line?
          if (startsWith(buffer, "vn "))
            {
              //parse into 3 doubles
              normals.push_back(parseToVector3f(buffer));
            }
          //is texcoord line?
          if (startsWith(buffer, "vt "))
            {
              //parse into 2 doubles
              tex_coord.push_back(parseToVector2f(buffer));
            }
          //is face line?
          if (startsWith(buffer, "f "))
            {
              //parse into ints
              parseFaceLine(buffer);
            }

          //printf("%s", buffer);
        }
      while (c != EOF);
      fclose(f);
    }
  else
    {
      std::cout << "File \"" << path << "\" not found!" << std::endl;
    }
  free(buffer);
  sortTexCoordAndNormals();

  Mesh* mesh = new Mesh(&vertices, &indexes, &tex_coord, &normals);
  return mesh;
}

void
WaveFrontLoader::parseFaceLine(char* line)
{
  //"f 226/50/218 225/1/218 227/54/218"
  std::vector<char*> ch = splitByDelimiter(line, " ");
  for (int i = 1; i < 4; i++)
    {//3 durchläufe
      if (equals(ch[i], ""))
        {
          continue;
        }
      //std::cout<<"line: "<<ch[i]<<std::endl;
      std::vector<char*> chf = splitByDelimiter(ch[i], "/");
      int n0 = atoi(chf[0]);
      if (n0 == 0)
        {
          continue;
        }
      //substract 1 because the counting in the programm starts at 0,
      //not at 1 like in the file
      indexes.push_back(atoi(chf[0]) - 1);

      /*read only the first number for indexes, because the others are not needed
       * nr_vertice/nr_texCoord/nr_normal
       * a vertice has always the same texCoord and normal
      */
      int n1 = atoi(chf[1])-1;
      int n2=0;
      if (chf.size() > 2)
        {
          n2=atoi(chf[2]) - 1;
        }
      Vector3f* face=new Vector3f(n0,n1,n2);
      faces.push_back(face);
    }

}
/**
 * Returns the face information (texCoord nr and normal nr) for the vertice with the given nr.
 */
Vector3f* WaveFrontLoader::getFace(int vertnr){
  for(int i=0;i<faces.size();i++){
      if(faces[i]->x==vertnr){
          return faces[i];
      }
  }
  return NULL;
}
/**
 * Sorts the texture coordinates and normals like in described in faces.
 * (May not work correctly. You can comment it out, so only texCoords and normals may be wrong.)
 */
void WaveFrontLoader::sortTexCoordAndNormals(){
  std::vector<Vector2f*> old_tex_coord;
  std::vector<Vector3f*> old_normals;
  for(int i=0;i<tex_coord.size();i++){
      Vector2f* vec=new Vector2f(tex_coord[i]->x,tex_coord[i]->y);
      old_tex_coord.push_back(vec);
  }
  for(int i=0;i<normals.size();i++){
      Vector3f* vec=new Vector3f(normals[i]->x,normals[i]->y,normals[i]->z);
      old_normals.push_back(vec);
  }

  //sort tex_coord and normals new
  //vertice nr starts with 0
  for(int i=0;i<vertices.size();i++){
      Vector3f* face=getFace(i);
      if(face==NULL){
          continue;
      }
      if(face->y!=-1){
        Vector2f* vec_t=new Vector2f(old_tex_coord[face->y]->x,old_tex_coord[face->y]->y);
        tex_coord[i]=vec_t;
        //std::cout<<vec_t->x<<","<<vec_t->y<<std::endl;
      }
      if(face->z!=-1){
        Vector3f* vec_n=new Vector3f(old_normals[face->z]->x,old_normals[face->z]->y,old_normals[face->z]->z);
        normals[i]=vec_n;
        //std::cout<<vec_n->x<<","<<vec_n->y<<","<<vec_n->z<<std::endl;
      }
  }
}
Vector2f*
WaveFrontLoader::parseToVector2f(char* line)
{
  //"vt -0.997900 0.065500"
  std::vector<char*> ch = splitByDelimiter(line, " ");
  Vector2f* vec = new Vector2f(atof(ch[1]), atof(ch[2]));
  //std::cout<<vec->x<<","<<vec->y<<std::endl;
  return vec;
}
Vector3f*
WaveFrontLoader::parseToVector3f(char* line)
{
  //"(v/vn) -0.997900 0.065500 0.000000"
  std::vector<char*> ch = splitByDelimiter(line, " ");
  Vector3f* vec = new Vector3f(atof(ch[1]), atof(ch[2]), atof(ch[3]));
  //std::cout<<vec->x<<","<<vec->y<<","<<vec->z<<std::endl;
  return vec;
}

