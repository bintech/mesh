/*
 * stringtools.cpp
 *
 *  Created on: 28.03.2015
 *      Author: binchi
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <iostream>

#include "stringtools.hpp"

/**
 * Checks if the path has the correct ending.
 */
bool
endsWith(const std::string &str, const std::string &suffix)
{
  return str.size() >= suffix.size() && str.compare(str.size() - suffix.size(),
      suffix.size(), suffix) == 0;
}
bool
startsWith(char* line, char* prefix)
{
  const char* pre = strstr(line, prefix);
  if (line == pre)
    {
      return true;
    }
  return false;
}

bool
equals(char* a, char* b)
{
  return (strcmp(a, b) == 0);
}
/**
 * Splits the line into subparts which will be written into a vector and will be returned.
 * If delimiter!=" ", you need to do a free for all vector elements.
 */
std::vector<char*>
splitByDelimiter(char* line, char* delimiter)
{
  std::vector<char*> ch;
  std::string delim(delimiter);
  std::string lin(line);
  std::size_t st;
  //std::cout<<line<<": "<<std::endl;
  do
    {
      std::string sub = lin.substr(0, lin.find(delim));
      char* c = (char*) sub.c_str();
      char* c1 = (char*) malloc(strlen(c) + 1);
      strcpy(c1, (char*) sub.c_str());
      ch.push_back(c1);
      st = lin.find(delim);
      lin = lin.replace(lin.find(sub), sub.length(), "");
      if (lin.find(delim) != std::string::npos)
        {
          lin = lin.replace(lin.find(delim), delim.length(), "");
        }
      else
        {
          break;
        }
      //std::cout<<c<<", "<<lin<<std::endl;
    }
  while (st != std::string::npos);
  const char* c = lin.c_str();
  ch.push_back((char*) c);

  /*for(int i=0;i<ch.size();i++){
   printf ("%s,",ch[i]);
   }printf ("\n");*/
  return ch;
}
char*
toChar(std::string s)
{
  return (char*) s.c_str();
}
std::string
toString(char* s)
{
  std::string str(s);
  return str;
}
