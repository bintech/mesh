/*
 * ObjectType.hpp
 *
 *  Created on: 25.03.2015
 *      Author: binchi
 */

#ifndef OBJECTTYPE_HPP_
#define OBJECTTYPE_HPP_

#include "../data/block.h"

class ObjectType{
	private:
		enum block type;//identifier
		char* mesh;//path to the mesh, if mesh==null -> box
		char* texture;//path to the texture, if texture==null -> no texture

	public:
		ObjectType(block ntype,char* nmesh,char* ntexture);
		enum block getType();
		char* getMesh();
		char* getTexture();

};

#endif
